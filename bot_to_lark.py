#coding:utf-8

import requests
import json
import datetime
import os
from apscheduler.schedulers.blocking import BlockingScheduler
import config
import get_date

url_join = "https://oapi.zjurl.cn/open-apis/api/v2/bot/chat/join"
url_message = "https://oapi.zjurl.cn/open-apis/api/v2/message"

email_list = ["yanyi.qf@bytedance.com","zhoulong.qa@bytedance.com","shenpanpan@bytedance.com","liyang.921@bytedance.com","zhangxinjia@bytedance.com","jialiwen@bytedance.com","zhangxin.yuan@bytedance.com","daijiaqian@bytedance.com"]



# def bot_joinin_lark():
#     """
#     发送lark群消息
#     :param title:
#     :param text:
#     :return:
#     """
#     data= {
#     "token": "u-d340a48a-3178-4644-ae39-8b52f4cebb8f",
#     "bot": "b-3e6b5d0b-23aa-433b-bc83-442541fa9853",
#     "chat_id": "6665942008927879428"
# }
#     headers = {'Content-type': 'application/json'}
#     r = requests.post(url=url_join, data=json.dumps(data), headers=headers)
#     if r.status_code > 200:
#         return None
#     resp_data = r.json()
#     if not resp_data.get("ok") or not resp_data.get("channel"):
#         return None
#     chat_id = resp_data.get("channel").get("id")
#     return chat_id

# 根据email查询对应的用于lark用户uid
def get_uid_info(email_list):
    uid_list = []
    for i in email_list:
        url = 'https://oapi.zjurl.cn/open-apis/api/v1/user.user_id'
        payload = {
            "token": 'b-3e6b5d0b-23aa-433b-bc83-442541fa9853',
            "email": i ,
        }
        payload = json.dumps(payload)
        re = requests.post(url, data=payload).content
        data = json.loads(re)
        uid_list.append(str(data['user_id']))
    return uid_list

def send_text(uid):
    str = ''
    str +=  '<at user_id=' + uid + '></at>\n'+'哈哈哈'
    return str

# 根据email查询对应的用于lark用户uid
# 单个用户
def get_single_uid_info(email):
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/user.user_id'
    payload = {
            "token": 'b-3e6b5d0b-23aa-433b-bc83-442541fa9853',
            "email": email ,
            }
    payload = json.dumps(payload)
    re = requests.post(url, data=payload).content
    data = json.loads(re)
    return str(data['user_id'])

def send_text(uid):
    str = ''
    str +=  '<at user_id=' + uid + '></at>\n'+'本周的用户反馈该你回了呦'
    return str

def send_message(text):
    data= {
        "token": "b-3e6b5d0b-23aa-433b-bc83-442541fa9853",
        "chat_id": "6665942008927879428",
        "msg_type": "post",
        "content": {
                "text": text,
                "title": "用户反馈提醒",
                }
    }

    headers = {'Content-type': 'application/json'}

    r = requests.post(url=url_message, data=json.dumps(data), headers=headers)

    # if r.status_code > 200:
    #     print ("post message to lark-apis failed: r= %s", r)
    #     return False
 
   # print ("post message to lark-channel success: chat_id=%s, content=%s",(data.get('chat_id'), data.get('content')))
    # return True

def time_task(weekday,hour,minute,second):
    while True:
        now = datetime.datetime.now()
        week_weekday = get_date.get_weekday(now)
        week_hour = get_date.get_hour(now)
        week_minute = get_date.get_minute(now)
        week_second = get_date.get_sec(now)
        # print(week_weekday,week_hour,week_minute)
        if week_weekday == weekday and week_hour == hour and week_minute == minute and week_second == second:
            print("=====")
            # print(now.month)
            user = config.date_dict.keys()
            now_date = get_date.get_date(now)

            for i in user:
                # print(i)
                if config.date_dict[i] == now_date:
                    email = (config.email_dict[i])
                    uid = get_single_uid_info(email)
                    text = send_text(uid)
                    send_message(text)

# now = datetime.datetime.now()
# print(now.month)
# user = config.date_dict.keys()
# now_date = get_date.get_date(now)

# for i in user:
#     print(i)
#     if config.date_dict[i] == now_date:
#         email = (config.email_dict[i])
#         uid = get_single_uid_info(email)
#         text = send_text(uid)
#         send_message(text)


# uid_list = get_uid_info(email_list)

# for i in uid_list:
#     text = send_text(i)
#     send_message(text)

# send_message()
# bot_joinin_lark()
if __name__ == '__main__':
    time_task(0,11,0,0)
