#coding:utf-8

import requests
import json
from datetime import datetime
import os
from apscheduler.schedulers.blocking import BlockingScheduler

Web_hook_url = "https://oapi.zjurl.cn/open-apis/bot/hook/d81bc80e3d5c4dacb3d32028fad323d2"



def send_message(title,text):
    """
    发送lark群消息
    :param title:
    :param text:
    :return:
    """
    data= {
        'title': title,
        'text': text
    }
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(url=Web_hook_url, data=json.dumps(data), headers=headers)
    if r.status_code > 200:
        return None
    resp_data = r.json()
    if not resp_data.get("ok") or not resp_data.get("channel"):
        return None
    chat_id = resp_data.get("channel").get("id")
    return chat_id


# send_message("hello","lark custom bot")



if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(send_message("hello","custom bot"), 'cron', hour=22,minute=17)
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass