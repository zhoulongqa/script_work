email_dict = {
	"颜一" : "yanyi.qf@bytedance.com",
	"周龙" : "zhoulong.qa@bytedance.com",
	"申盼盼" : "shenpanpan@bytedance.com",
	"李杨" : "liyang.921@bytedance.com",
	"张新佳" : "zhangxinjia@bytedance.com",
	"贾立文" : "jialiwen@bytedance.com",
	"张新" : "zhangxin.yuan@bytedance.com",
	"代佳倩" : "daijiaqian@bytedance.com",
}

date_dict = {
	"颜一" : "4-29",
	"周龙" : "4-27",
	"申盼盼" : "5-13",
	"李杨" : "5-20",
	"张新佳" : "5-27",
	"贾立文" : "6-3",
	"张新" : "6-10",
	"代佳倩" : "6-17",
}