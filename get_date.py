#coding:utf-8

import datetime


def get_month(date):
	return date.month

def get_day(date):
	return date.day

def get_hour(date):
	return date.hour

def get_minute(date):
	return date.minute

def get_sec(date):
	return date.second

def get_weekday(date):
	return date.weekday()


def get_date(date):
	return str(get_month(date)) + '-' + str(get_day(date))

def get_time(date):
	return str(get_hour(date)) + ":" + str(get_minute(date)) + ":" + str(get_sec(date))



if __name__ == '__main__':
	while True:
		now = datetime.datetime.now()
		print(get_date(now))
		print(get_time(now))
		print(get_weekday(now))
		print('--------------')
		print(type(get_weekday(now)))
		print(type(get_hour(now)))
		if get_time(now) == '12:36:0':
			print("get it")
			break
